import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'ngx-webstorage';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  constructor(private userService : UserService, private route : Router, private toast : ToastrService, private storage : LocalStorageService){
    
  }

  loginDetails = new FormGroup({
    email : new FormControl('', [Validators.required, Validators.email]),
    password : new FormControl('', [Validators.required, Validators.minLength(8)])
  })

  login(){
    
    const {email, password} = this.loginDetails.value;
    
    this.userService.loginValidations(email, password).subscribe((data:any)=>{
      const {id, status, userName} = data;
      
      if(status){
        localStorage.setItem("username", userName); 
        this.userService.setUserLogedIn();
        this.toast.success('Logged in successfully','',{
          positionClass:'toast-bottom-right',
          timeOut : 1000
        }); 
        this.route.navigate(['dashboard']);
        this.storage.store("status", true);
        localStorage.setItem('id', id);            
      } else if(id != 0) {
        this.toast.error('invalid password','',{
          positionClass:'toast-bottom-right',
          timeOut : 3000
        })
      } else {
        this.toast.error('invalid email','',{
          positionClass:'toast-bottom-right',
          timeOut : 3000
        })
      }
    })
  }
}
