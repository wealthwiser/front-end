import { Component } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import {FormControl, FormGroup, Validators} from '@angular/forms'
import { UserService } from '../user.service';

@Component({
  selector: 'app-saving-goals',
  templateUrl: './saving-goals.component.html',
  styleUrl: './saving-goals.component.css'
})
export class SavingGoalsComponent {
  allGoalDetails : any = [];
  perticularDetails : any = {};
  userId = localStorage.getItem("id");
  constructor(private service : UserService) {
    this.getAllGoals();
  }

  updateGoalDetails = new FormGroup({
    title : new FormControl('', [Validators.required]),
    amount : new FormControl('', Validators.required),
    saved : new FormControl('', Validators.required)
  })

  goalDetails = new FormGroup({
    title : new FormControl('', [Validators.required]),
    amount : new FormControl('', Validators.required),
    saved : new FormControl('', Validators.required)
  })

  getAllGoals(){
    this.service.getGoalsByUserId(this.userId).subscribe((res)=>{
      this.allGoalDetails = res;
    })
  }

  add(){
    const {title, amount, saved} = this.goalDetails.value;
    this.service.setGoal({userId : this.userId, title, amount, saved}).subscribe((res)=>{
      this.getAllGoals();
    })
    this.goalDetails.reset();
  }

  triggerUpdate(id:any){
    
    for(let goal of this.allGoalDetails){
      if(goal.id == id){
        this.perticularDetails = goal;
      }
    }
    this.updateGoalDetails = new FormGroup({
      title : new FormControl(this.perticularDetails.title, [Validators.required]),
      amount : new FormControl(this.perticularDetails.amount, Validators.required),
      saved : new FormControl(this.perticularDetails.saved, Validators.required)
    })
  }

  update(){
    const {title, amount, saved} = this.updateGoalDetails.value;
    this.service.updateGoal({id:this.perticularDetails.id, userId : this.userId, title, amount, saved}).subscribe((res)=>{
      this.getAllGoals();
    })
  }

  delete(id:any){
    this.service.deleteGoalById(id).subscribe((res)=>{
      this.getAllGoals();
    })
  }
}
