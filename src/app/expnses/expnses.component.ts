import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-expnses',
  templateUrl: './expnses.component.html',
  styleUrl: './expnses.component.css'
})
export class ExpnsesComponent implements OnInit{
  expenses : any = [];
  oneExpesne : any = {};
  expesnseId : any;
  updateExpenseDetails = new FormGroup({
      updateTitle : new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(25)]),
      updateCategory : new FormControl('0',[Validators.required]),
      updateDescription : new FormControl('',[Validators.required, Validators.maxLength(60)]),
      updateAmount : new FormControl('',[Validators.required]),
      updateDate : new FormControl('', [Validators.required])
  });

  expenseDetails = new FormGroup({
    title : new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(25)]),
    category : new FormControl('0',[Validators.required]),
    description : new FormControl('',[Validators.required, Validators.maxLength(60)]),
    amount : new FormControl('',[Validators.required]),
    date : new FormControl('', [Validators.required])
  })


  constructor(private service : UserService){
    
  }
  ngOnInit(): void {
    this.service.getAllExpense(localStorage.getItem('id')).subscribe((data)=>{
      this.expenses = data;
    })
  }

  getAllDetails(){
    this.service.getAllExpense(localStorage.getItem('id')).subscribe((data)=>{
      this.expenses = data;
    })
  }
  
  submitExpense(){
    const {title, category, description, amount, date} = this.expenseDetails.value;
    console.log(date);
    
    let userId = localStorage.getItem('id');
    this.service.addExpense({userId, title, category, description, amount, date}).subscribe((res)=>{
      this.getAllDetails();
    })

    this.expenseDetails.reset();
  }

  deleteExpense(id:any){
    this.service.deleteExpense(id).subscribe((res)=>{
      this.getAllDetails();
    })
  }

  activateModalUpdateExpense(id:any){
    this.expesnseId = id;
    for(let item of this.expenses){
      if(item.id == id){
        this.oneExpesne = item;
        break;
      }
    }
    console.log(this.oneExpesne);
    

    this.updateExpenseDetails = new FormGroup({
      updateTitle : new FormControl(this.oneExpesne.title, [Validators.required, Validators.minLength(5), Validators.maxLength(25)]),
      updateCategory : new FormControl(this.oneExpesne.category,[Validators.required]),
      updateDescription : new FormControl(this.oneExpesne.description,[Validators.required, Validators.maxLength(60)]),
      updateAmount : new FormControl(''+this.oneExpesne.amount,[Validators.required]),
      updateDate : new FormControl(this.oneExpesne.date, [Validators.required])
    })

    
  }

  updateDetails(){
    const {updateTitle: title, updateCategory : category, updateDescription : description, updateAmount : amount, updateDate : date} = this.updateExpenseDetails.value;
    let userId = localStorage.getItem('id');
    let id = this.expesnseId;
    this.service.updateExpense({id, userId, title, category, description, amount, date}).subscribe((res)=>{
      this.getAllDetails();
    })
  }
}
