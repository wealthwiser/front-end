import { ChangeDetectorRef, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { ImplicitReceiver } from '@angular/compiler';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrl: './nav-bar.component.css'
})
export class NavBarComponent {
  username : any = localStorage.getItem("username");
  constructor(public service : UserService, private route : Router, private storage : LocalStorageService, private cdr: ChangeDetectorRef){
    this.username = localStorage.getItem("username");    
  }
  
  

  logout(){
    this.storage.clear("status");
    localStorage.removeItem("username");
    this.service.setUserLogedOut();
    this.route.navigate(['']);
  }
}
