import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Chart } from 'angular-highcharts'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.css'
})
export class DashboardComponent implements OnInit {
  balanceDetails : any = {
    id: 0,
    userId: 0,
    balance: 0,
    month: ""
  };
  currMonth = new Date().getMonth() + 1;
  monthExpenses : any = [];
  spent : number = 0;
  remaining : number = 0;
  pieChart : any;
  savingGoals : any = [];
  constructor(private service : UserService) {
  }
  ngOnInit(): void {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const month = new Date().getMonth();
    const monthName = monthNames[month];
    this.service.getBalanceByMonth(monthName, localStorage.getItem('id')).subscribe((res)=>{
      if(res != null){
        this.balanceDetails = res;
      }
     
    })
    this.service.getExpenseByMonth(this.currMonth, localStorage.getItem('id')).subscribe((res)=>{
      this.monthExpenses = res;
      this.calculateExpenseInfo();
    })
    this.service.getGoalsByUserId(localStorage.getItem('id')).subscribe((res)=>{
      this.savingGoals = res;
    })
  }

  calculateExpenseInfo(){
    let shopping = 0;
    let food = 0;
    let traveling = 0;
    let bills = 0;
    let rent = 0;
    let maintenance = 0;
    let other = 0;
    for(let expense of this.monthExpenses){
      this.spent += expense.amount;
      if(expense.category == "Shopping"){
        shopping += expense.amount;
      }
      if(expense.category == "Food"){
        food += expense.amount;
      }
      if(expense.category == "Traveling"){
        traveling += expense.amount;
      }
      if(expense.category == "Bills"){
        bills += expense.amount;
      }
      if(expense.category == "Rent"){
        rent += expense.amount;
      }
      if(expense.category == "Maintaince"){
        maintenance += expense.amount;
      }
      if(expense.category == "Other"){
        other += expense.amount;
      }
    }
    this.remaining = this.balanceDetails.balance - this.spent;
    this.pieChart = new Chart({
      chart : {
        type : 'column'
      },
      xAxis: {
        categories: ["Shopping", "Food", "Traveling", "Bills", "Rents", "Maintenance", "Other"]
      },
      title : {
        text : ""
      },
      credits : {
        enabled : false
      },
      legend : {
        enabled : false
      }, 
      series : [{
        type : 'column',
        data : [shopping, food, traveling, bills, rent, maintenance, other]
      }]
    })
  }

  
}


