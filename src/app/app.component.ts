import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit {
  constructor(private route : Router, private user : UserService, private storage : LocalStorageService){
        
  }
  ngOnInit(): void {
    if(this.storage.retrieve("status")){
      this.user.setUserLogedIn();
      this.route.navigate(['dashboard']);
    }
  }
  
}
