import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { authGuard } from './auth.guard';
import { NotFoundComponent } from './not-found/not-found.component';
import { ExpnsesComponent } from './expnses/expnses.component';
import { SavingGoalsComponent } from './saving-goals/saving-goals.component';
import { AccountComponent } from './account/account.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'login', component:LoginComponent},
  {path:'register', component:RegisterComponent},
  {path:'dashboard', canActivate:[authGuard], component:DashboardComponent},
  {path:'expenses', canActivate:[authGuard], component:ExpnsesComponent},
  {path:'goals', canActivate:[authGuard], component: SavingGoalsComponent},
  {path:'account', canActivate:[authGuard], component: AccountComponent},
  {path:'**', component:NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
