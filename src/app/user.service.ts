import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isUserLogedIn : boolean;
  constructor(private http : HttpClient) { 
    this.isUserLogedIn = false;
  }
  setUserLogedIn(){
    this.isUserLogedIn = true;
  }
  setUserLogedOut(){
    this.isUserLogedIn = false;
  }
  getStatus(){
    return this.isUserLogedIn;
  }
  loginValidations(email : any, password : any){
    return this.http.get('http://localhost:8085/user-login/'+email+"/"+password);
  }
  registerUser(body:any){
    return this.http.post('http://localhost:8085/register-user',body,{responseType : 'text'});
  }
  getAllExpense(userId:any){
    return this.http.get('http://localhost:8085/get-expense-by-userid/'+userId);
  }
  addExpense(body:any){
    return this.http.post('http://localhost:8085/add-expense',body,{responseType : 'text'})
  }

  deleteExpense(id:any){
    return this.http.delete('http://localhost:8085/delete-expense/'+id,{responseType : 'text'});
  }
  updateExpense(body:any){
    return this.http.put('http://localhost:8085/update-expense',body,{responseType : 'text'});
  }
  getExpenseById(id:any){
    return this.http.get('http://localhost:8085/get-expense-by-id/'+id);
  }
  getExpenseByMonth(currMonth:any, userId:any){
    return this.http.get('http://localhost:8085/get-expense-by-month/'+currMonth+"/"+userId);
  }
  setBalance(body:any){
    return this.http.post("http://localhost:8085/set-balance", body, {responseType:'text'});
  }
  getBalance(userId:any){
    return this.http.get("http://localhost:8085/get-balance-details/"+userId);
  }
  getBalanceByMonth(month:any, userId:any){
    return this.http.get("http://localhost:8085/get-balance-by-month/"+month+"/"+userId);
  }
  deleteBalance(id:any){
    return this.http.delete("http://localhost:8085/delete-balance/"+id, {responseType: 'text'})
  }
  getGoalsByUserId(userId:any){
    return this.http.get("http://localhost:8085/get-all-saving-goals/"+userId);
  }
  setGoal(body:any){
    return this.http.post("http://localhost:8085/add-saving-goal", body, {responseType: 'text'});
  }
  deleteGoalById(id:any){
    return this.http.delete("http://localhost:8085/delete-saving-goal/"+id, {responseType: 'text'});
  }
  updateGoal(body:any){
    return this.http.put("http://localhost:8085/update-saving-goal", body, {responseType:'text'});
  }
  updatePassword(userId:any, oldPass:any, newPass:any){
    return this.http.put("http://localhost:8085/update-password/"+userId+"/"+oldPass+"/"+newPass,{},{responseType : 'text'});
  }
}
