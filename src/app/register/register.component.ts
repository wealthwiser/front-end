import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css',
})

export class RegisterComponent {
  constructor(private userService: UserService, private route : Router, private toast : ToastrService) {
    this.siteKey = '6LfUOtMpAAAAAELqN4YElxPBIMsItw4MYo1tCNY3';
  }
  siteKey : string;
  registerForm = new FormGroup({
    name : new FormControl('', [Validators.required, Validators.minLength(6)]),
    username : new FormControl('', [Validators.required, Validators.minLength(6)]),
    email : new FormControl('', [Validators.required, Validators.email]),
    password : new FormControl('', [Validators.required, Validators.minLength(8)]),
    cpassword : new FormControl('')
  })


  submitForm() {
    const {name, email, password, username} = this.registerForm.value;
    
    // console.log(name + " : " + username + " : " + email + " : " + password);
    let userName = username;
    this.userService.registerUser({name, email, password, userName}).subscribe((res) => {
      this.toast.success('Registration successfully','',{
        positionClass:'toast-bottom-right',
        timeOut : 1000
      });
      this.route.navigate(['login']);
    });
  }
}
