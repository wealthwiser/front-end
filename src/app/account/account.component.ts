import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrl: './account.component.css'
})
export class AccountComponent {
  months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"];
  allBalanceDetails : any ;
  constructor(private service : UserService, private toast : ToastrService) { 
    this.getDetails();
   }
  addBalance = new FormGroup({
    balance : new FormControl('', [Validators.required]),
    month : new FormControl('0', [Validators.required])
  })

  updatePassword = new FormGroup({
    oldPass : new FormControl('',[Validators.required, Validators.minLength(8)]),
    newPass : new FormControl('',[Validators.required, Validators.minLength(8)]),
    cPass : new FormControl('', [Validators.required])
  })

  updatePass(){
    const {oldPass, newPass} = this.updatePassword.value;
    this.service.updatePassword(localStorage.getItem('id'), oldPass, newPass).subscribe((res)=>{
      
      if((/true/).test(res)){
        this.toast.success('password updated successfully','',{
          positionClass:'toast-bottom-right',
          timeOut : 2000
        });
        this.updatePassword.reset();
      } else {
        this.toast.error('Incorrect password','',{
          positionClass:'toast-bottom-right',
          timeOut : 2000
        });
      }
    })
  }
  getDetails(){
    this.service.getBalance(localStorage.getItem('id')).subscribe((res)=>{
      this.allBalanceDetails = res;
    })
  }
  add(){
    let userId = localStorage.getItem('id');
    const {balance, month} = this.addBalance.value;
    let flag = true;
    for(let item of this.allBalanceDetails){
      if(item.month == month){
        this.toast.error('This month limit already exist','',{
          positionClass:'toast-top-right',
          timeOut : 2000
        });
        flag = false;
      } 
    }

    if(flag){
      this.service.setBalance({userId, balance, month}).subscribe((res)=>{
        this.getDetails();
      });
    }
    this.addBalance.reset();
  }
  delete(id:any){
    this.service.deleteBalance(id).subscribe((res)=>{
      this.getDetails();
    })
  }
}
